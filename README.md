**Task**

"Create a war that receives POST commands (using Spring controllers) and saves the body of the post to the file system and posts an even to a RabbitMQ queue."

**Prerequisites**

- Install RabbitMQ with default settings. It should be available by the link http://localhost:15672/mgmt with creds Username: guest / Password: guest

**Checkout:**

Attached jar file target\rabbitmq-java-test.jar to run application.

Copy jar to any place.

**Build application:**

cd to project directory.

run **_mvn clean install_** command to build jar file.

**Run Spring boot application**

run java -jar {jar_path}/rabbitmq-java-test.jar

Aplication is available by http://localhost:8087/test . 

Using Advanced Rest Client tool test endpoint _POST /test_ with payload:

`{
   "name": "arhangel18@ukr.net",
   "email": "arhangel18@ukr.net",
   "password": "1234567890",
   "passwordRepeated": "1234567890"
 }`


**Console output**

After posting request you should have following lines in console:

`2016-09-20 17:51:59.930  INFO 19980 --- [nio-8087-exec-5] c.j.controller.UserFormController        : Processing was started with userForm=UserForm{name='arhangel18@ukr.net', email='arhangel18@ukr.net', password='1234567890', passwordRepeated='1234567890'}, bind
 ingResult=org.springframework.validation.BeanPropertyBindingResult: 0 errors
 2016-09-20 17:51:59.931  INFO 19980 --- [nio-8087-exec-5] com.javatest.service.UserFormService     : Sending to RabbitMQ following=UserForm{name='arhangel18@ukr.net', email='arhangel18@ukr.net', password='1234567890', passwordRepeated='1234567890'}
 2016-09-20 17:51:59.932  INFO 19980 --- [nio-8087-exec-5] com.javatest.service.UserFormService     : Sending to RabbitMQ was completed
 2016-09-20 17:51:59.933  INFO 19980 --- [nio-8087-exec-5] com.javatest.service.UserFormService     : Output file was created = D:\Test\Git\rabbitmq-java-test\output.txt
 2016-09-20 17:51:59.935  INFO 19980 --- [nio-8087-exec-5] c.j.controller.UserFormController        : Processing was finished
 2016-09-20 17:51:59.938  INFO 19980 --- [    container-1] c.j.service.receiver.MessageReceiver     : Message Received:
  ={
   "name" : "arhangel18@ukr.net",
   "email" : "arhangel18@ukr.net",
   "password" : "1234567890",
   "passwordRepeated" : "1234567890"
 }
`

