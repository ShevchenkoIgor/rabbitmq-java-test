package com.javatest.validator;

import com.javatest.dto.UserForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Validator for request payload
 *
 * @author Shevchenko Igor
 * @since 2016-09-20
 */
@Component
public class UserFormValidator implements Validator{
    private static final Logger LOGGER = LoggerFactory.getLogger(UserFormValidator.class);

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(UserForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        LOGGER.debug("Validating {}", target);
        UserForm  user = (UserForm ) target;
        validatePasswords(errors, user);
        validateEmail(errors, user);
    }

    private void validatePasswords(Errors errors, UserForm  user) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.reject("password", "Passwords Size is not correct");
        }

        if (!user.getPassword().equals(user.getPasswordRepeated())) {
            errors.reject("password.no_match", "Passwords do not match reject");
        }
    }

    private void validateEmail(Errors errors, UserForm  user) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (user.getEmail().length() < 6 || user.getEmail().length() > 32) {
            errors.rejectValue("email", "Email Size is not correct");
        }
    }
}
