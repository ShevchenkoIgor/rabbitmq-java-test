package com.javatest.service.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;

/**
 * Class for receive messages from RabbitMQ
 *
 * @author Shevchenko Igor
 * @since 2016-09-20
 */
public class MessageReceiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageReceiver.class);
    private CountDownLatch countDownLatch = new CountDownLatch(1);

    public void receiveMsg(String message) {
        LOGGER.info("Message Received: \n ={}", message);
        countDownLatch.countDown();
    }

    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }
}