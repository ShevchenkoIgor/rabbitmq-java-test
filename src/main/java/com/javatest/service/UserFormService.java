package com.javatest.service;

import com.javatest.dto.UserForm;
import com.javatest.service.receiver.MessageReceiver;
import com.javatest.util.UserFormUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

/**
 * Service class for UserForm processing
 *
 * @author Shevchenko Igor
 * @since 2016-09-20
 */
@Service
public class UserFormService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserFormService.class);

    @Value("${queue.name}")
    private String queueName;

    @Value("${file.name}")
    private String fileName;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MessageReceiver messageReceiver;

    @Autowired
    private UserFormUtil userFormUtil;

    /**
     * Method sends object as string to RabbitMQ
     * @param userForm object which should be sent
     */
    public void sendToRabbitMQ(UserForm userForm) throws InterruptedException {
        LOGGER.info("Sending to RabbitMQ following={}", userForm);

        rabbitTemplate.convertAndSend(queueName, userFormUtil.convertObjectToJsonString(userForm));

        LOGGER.info("Sending to RabbitMQ was completed");

        messageReceiver.getCountDownLatch().await(5, TimeUnit.SECONDS);
    }

    /**
     * Method saves object as string to File system
     * @param userForm object which should be saved
     */
    public void saveToFileSystem(UserForm userForm) {
        try {
            File newTextFile = new File(fileName);

            FileWriter fw = new FileWriter(newTextFile);
            fw.write(userFormUtil.convertObjectToJsonString(userForm));
            fw.close();

            LOGGER.info("Output file was created = {}", newTextFile.getAbsolutePath());

        } catch (IOException iox) {
            //do stuff with exception
            iox.printStackTrace();
        }
    }
}
