package com.javatest.controller;

import com.javatest.dto.UserForm;
import com.javatest.service.UserFormService;
import com.javatest.validator.UserFormValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Controller class with Rest endpoint
 *
 * @author Shevchenko Igor
 * @since 2016-09-20
 */
@RequestMapping("/test")
@RestController
public class UserFormController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserFormController.class);

    private final UserFormService userFormService;
    private final UserFormValidator userFormValidator;

    @Autowired
    public UserFormController(UserFormService userFormService, UserFormValidator userFormValidator) {
        this.userFormService = userFormService;
        this.userFormValidator = userFormValidator;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(userFormValidator);
    }

    /**
     * Resource Method
     * @param userForm
     * @param bindingResult
     * @return
     * @throws Exception
     */
    @RequestMapping(method = POST)
    public ResponseEntity getUser(@Valid @RequestBody final UserForm userForm, BindingResult bindingResult) throws Exception {
        LOGGER.info("Processing was started with userForm={}, bindingResult={}", userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }

        userFormService.sendToRabbitMQ(userForm);
        userFormService.saveToFileSystem(userForm);

        LOGGER.info("Processing was finished");
        return ResponseEntity.ok("Success");
    }
}
